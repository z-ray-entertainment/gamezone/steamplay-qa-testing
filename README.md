# SteamPlay/Proton/Steam Deck QA Testing

This is a public issue board about game compatibility with the translation layer Proton which is used by Steam to run Windows games on Linux.  

Here you an find various games we re-test on a reqular basis until they work as expected on Linux with/without Proton.  

The goal of this project is to allow for reproducatbile and relaiable test.  
This is done in order to provide quality QA reprots for ProtonDB and to keep a list of tests required so nothign gets missing.  

Furher more this is to keep track of issues currently encountered by some games.  

Games which are Deck Verified are unlikley to be tested here.  

Games with native versions are tested as well in order to find out issues with a native game which may play better with Proton or vise versa.
