# About
Issue description  

⚠️Remove task which do not apply to this test⚠️

Use these symbols to mark tests:  
✅: Passed  
❌: Failed  
⚠️: Palatially failed  
🔳: Open/Untested

## Test: Launching
- 🔳 Launches in it's default state/configuration
- 🔳 Launches with forced Linux Runtime
- 🔳 Launches with forced Proton Runtime
- 🔳 Launches with the 3 most recent GE Proton builds (Order: DESC)
- 🔳 The following steps are required to make teh game run:

## Test: Controller
- 🔳 Works with controllers (Steam input: Default)
- 🔳 Works with controllers if Steam Input is forced: Off
- 🔳 Works with controllers if Steam Input is forced: On
- 🔳 Controller Buttons mapped correctly
- 🔳 Controller glyph are shown correctly
- 🔳 Does *not* suffer from Ghost input
- 🔳 Does stop responding to controller inputs if the Steam Overlay is shown
- 🔳 Does stop responding on controller input if the on-screen keyboard is shown
- 🔳 Allows for full controller interaction

## Test: Vsync
- 🔳 Enabling/disableing VSync works
- 🔳 Does not have build-in fps limit

## Test: Videos
- 🔳 Has videos (Pre-rendered cut scenes)
- 🔳 Cut-scenes play without modifications
- 🔳 Cut-scenes play with the 3 most recent GE Proton builds (Order DESC)
- 🔳 Language depending audio streams play correctly

## Test: Performance
⚠️ Everything below 60fps is considered a performance issue ⚠️  

- 🔳 Runs with at least 30fps
- 🔳 Runs with at least 60fps
- 🔳 Runs with at least 144fps
- 🔳 Made with Unity 3D
  - 🔳 `PROTON_NO_FSYNC=1 taskset -c 0,2,4,6 %command%` improves performance
- 🔳 `PROTON_NO_ESYNC=1 %command%`
  - 🔳 Runs with at least 30fps
  - 🔳 Runs with at least 60fps
  - 🔳 Runs with at least 144fps
- 🔳 `PROTON_NO_FSYNC=1 %command%`
  - 🔳 Runs with at least 30fps
  - 🔳 Runs with at least 60fps
  - 🔳 Runs with at least 144fps
- 🔳 A combination of the above is reqired to get better fps
  - 🔳 What combo was used here ...
  - 🔳 Runs with at least 30fps
  - 🔳 Runs with at least 60fps
  - 🔳 Runs with at least 144fps

## Test: Visuals
- 🔳 Does *not* suffer from flickering
- 🔳 Does *not* suffer from black textures
- 🔳 Does *not* suffer from distorted colours
- 🔳 Does *not* suffer from invisible geometry

## Test: Anti Cheat
- 🔳 Affected by Anti Cheat
- 🔳 Anti-cheat is suppoted on Steam Deck
- 🔳 Anti-Cheat is enabled for Steam Deck

## Test: Multiplayer
- 🔳 connects to an online game
- 🔳 Does *not* suffer from connection issues
- 🔳 Can host a game
- 🔳 Players can join a hosted game

## Test: Desktop
- 🔳 Tabbing out and the game works
- 🔳 Minimizing and maximizing the game works
- 🔳 Switching window modes works (Windowed/borderless Window/Fullscreen/Exlusive Fullscreen)
- 🔳 Does not react on keyboard, mouse and/or controller input when not active

### Tested configurations
- 🔳 Native
- 🔳 Vanilla Proton
- 🔳 Vanilla Proton Experimental
- 🔳 GE Proton Latest
- 🔳 GE Proton Latest-1
- 🔳 GE Proton Latest-2

### System configuration
- CPU: ...
- GPU: ...
  - Driver: ...
- RAM: ...
- OS: ...
- DE/WM: ...
- Display Server: X11/Wayland
- Primary Resolution: ...
